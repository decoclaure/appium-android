# Demo testing device with Appimen
en este ejemplo se muestra la extructura del proyect usando TestNG, ReportNG y Selenium

## Install **Appium** and **Maven**

http://appium.io/

http://maven.apache.org/download.cgi


## Run **Appium Server** in console

```js
appium --address 127.0.0.1 -p 4723
```

```js
[Appium] Welcome to Appium v1.13.0
[Appium] Non-default server args:
[Appium]   address: localhost
[Appium] Appium REST http interface listener started on localhost:4723
```

## To compile and run all tests, run:
```js
mvn clean install
```

## Archivo pom.xml
```
 <dependencies>
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>6.14.3</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <!-- reportng -->
        <!-- https://mvnrepository.com/artifact/org.uncommons/reportng genera el archivo html report --> 
        <dependency>
            <groupId>org.uncommons</groupId>
            <artifactId>reportng</artifactId>
            <version>1.1.4</version>
            <scope>test</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.google.inject/guice -->
        <dependency>
            <groupId>com.google.inject</groupId>
            <artifactId>guice</artifactId>
            <version>4.2.2</version>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-java</artifactId>
            <version>3.141.59</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>io.appium</groupId>
            <artifactId>java-client</artifactId>
            <version>7.0.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>27.1-jre</version>
        </dependency>
    </dependencies>
    
    <!-- plugint HTML resportNG -->
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.1</version>
                
                <configuration>			
                    <suiteXmlFiles>
                        <suiteXmlFile>./src/test/java/testNG/TestNG.xml</suiteXmlFile>
						
                    </suiteXmlFiles>
                </configuration>

            </plugin>
        </plugins>
    </build>

```