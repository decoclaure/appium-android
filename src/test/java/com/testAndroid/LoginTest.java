/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testAndroid;

import com.base.TestBase;
import java.io.IOException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

/**
 *
 * @author macminidev2
 */
public class LoginTest extends TestBase {

    public static int count;

    @Test(description = "Aplicando el primer test: ")
    public void loginTest() throws IOException, InterruptedException {
        System.out.println("\nAPLICANDO EL PRIMER TEST ");

        Reporter.log("PASS : <font color=\"green\"><b>" + "Test case has passed" + "</b></font><br/>");
        System.out.println("Test case has passed");
    }

    @Test(description = "Aplicando el segundo test: con Assert Fail: ")
    public void sessionOut() throws InterruptedException, IOException {
        System.out.println("\nAPLICANDO EL SEGUNDO TEST: ");

        //aplicado error 
        Assert.fail("demo error mostrando en caso de error en un dispositivo ");

        Reporter.log("PASS : <font color=\"green\"><b>" + "Test case has passed" + "</b></font><br/>");
        System.out.println("Test case has passed");

    }

    @Test(description = "Aplicando el tercer test:")
    public void proof() throws InterruptedException {
        Thread.sleep(1000);
        System.out.println("\nAPLICANDO EL tercer TEST ");

        Reporter.log("PASS : <font color=\"green\"><b>" + "Test case has passed" + "</b></font><br/>");
        System.out.println("Test case has passed");

    }

    @Test(description = "Aplicando el cuato test: ")
    public void prooff() throws InterruptedException {
        Thread.sleep(1000);

        System.out.println("curto Test sussesfully: ");

    }

}
