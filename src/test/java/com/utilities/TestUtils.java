/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utilities;

import static com.base.TestBase.appiumServer;
import static com.base.TestBase.config;
import static com.base.TestBase.driver;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;

/**
 *
 * @author macminidev2
 */
public class TestUtils {

    //screenshot capturing in test Failet
    public static Date nameimg = new Date();
    public static String imgName = null;

    //capture screenshot to device
    public static void captureScreenshot() throws IOException {

        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        Date da = new Date();

        imgName = da.toString().replace(" ", "_") + ".png";

        FileUtils.copyFile(srcFile, new File(System.getProperty("user.dir") + "/target/surefire-reports/html/" + imgName));

    }

    //devices verifications  connect to pc
    public Boolean adbDevicesVerifications(String device) throws IOException, InterruptedException {

        List<String> list = new ArrayList<>();

        Runtime runtime = Runtime.getRuntime();
        Process p = runtime.exec(new String[]{config.getProperty("pathADB"), "devices"}); //send comman adb devices
        p.waitFor();

        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";
        while ((line = reader.readLine()) != null) {

            String resutl = line.replace("device", "").replaceAll("\\s", ""); //remplasing all space 
            list.add(resutl);
        }
        int n = list.size();
        list.remove(n - 1);
        list.remove(0); //remove list deice cero

        if (list.contains(device)) {
       
            return true;
            
        } else {
            return false;
        }

    }

    //verification server appium up
    public Boolean serverAppiumUp(String serverPath) throws MalformedURLException, IOException {
        Boolean st = null;
        String url = serverPath + "/status";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        try {
            int responseCode = con.getResponseCode();

            if (responseCode == 200) {
                System.out.println("\nServer Appium => " + appiumServer + " Sucessfuly\n");
                st = true;
            } else {
                st = false;
            }
        } catch (IOException e) {
            System.err.println("\nServer Appium      " + serverPath + "       unable to connect");
            Assert.fail("Server Appium      " + serverPath + "       unable to connect");
        }
        return st;
    }

}
