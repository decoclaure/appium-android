/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listeners;


import com.utilities.TestUtils;
import static com.utilities.TestUtils.nameimg;
import java.io.IOException;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;


/**
 *
 * @author macminidev2
 */
public class Listeners implements ITestListener {

    @Override
    public void onTestStart(ITestResult itr) {

    }

    @Override
    public void onTestSuccess(ITestResult itr) {
        
    }

    @Override
    public void onTestFailure(ITestResult itr) {
       
        System.out.println("\nonTestFailure --> " + itr.getTestName());
        try {
            TestUtils.captureScreenshot();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }

        Reporter.log("Capturing screeshot error: ");
        Reporter.log("<a target=\"_blank\" href=\"" + TestUtils.imgName + "\">" + nameimg + "</a>");
        Reporter.log("<br>");
        Reporter.log("<br>");
        //Reporter.log("<br>");
        Reporter.log("<br>");
        Reporter.log("<a target=\"_blank\" href=" + TestUtils.imgName + "><img src=" + TestUtils.imgName + " height=700 width=400></img></a>");
        Reporter.log("<br>");
        Reporter.log("<br>");
        
    }

    @Override
    public void onTestSkipped(ITestResult itr) {
       

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult itr) {

    }

    @Override
    public void onStart(ITestContext itc) {

    }

    @Override
    public void onFinish(ITestContext itc) {

    }

}
