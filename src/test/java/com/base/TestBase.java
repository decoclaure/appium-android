/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.base;

import com.utilities.TestUtils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

/**
 *
 * @author Gustavo Claure Flores
 */
public class TestBase extends TestUtils {

    public static FileInputStream configProperties;
    public static Properties config = new Properties();
    public static DesiredCapabilities caps = new DesiredCapabilities();
    public static AppiumDriver<MobileElement> driver = null;
    public static String device = null;
    public static String appiumServer = null;
    public static WebDriverWait wait = null;

    @BeforeSuite
    public void setUp() throws FileNotFoundException, IOException, InterruptedException {

        //load file Config.properties
        configProperties = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/Config.properties");

        try {
            config.load(configProperties);
        } catch (IOException ex) {
            Logger.getLogger(TestBase.class.getName()).log(Level.SEVERE, null, ex);
        }

        //to accept html code in reportNG log
        System.setProperty("org.uncommons.reportng.escape-output", "false");


        //adding the value 'device' from jenkins
        if (System.getenv("device") != null && !System.getenv("device").isEmpty()) {

            device = System.getenv("device");

        } else {
            device = config.getProperty("device");

        }

        //adding 'serverAppium' value from jenkins
        if (System.getenv("serverAppium") != null && !System.getenv("serverAppium").isEmpty()) {
            appiumServer = System.getenv("serverAppium");
        } else {
            appiumServer = config.getProperty("serverAppium");
        }
        
        //value 'device' and 'serverAppium'
        config.setProperty("device", device);
        config.setProperty("serverAppium", appiumServer);

        int numDevice = 0;
        if (config.getProperty("device").equals("sony")) {
            //sony devise number => 1
            numDevice = 1;
            System.out.println("RUNNING TESTS ON DEVICE ==> " + config.getProperty("deviceName" + Integer.toString(numDevice)));
            
            if (adbDevicesVerifications(config.getProperty("udid" + Integer.toString(numDevice))) == true) {

                System.out.println("\nDevice " + config.getProperty("deviceName" + Integer.toString(numDevice)) + " connected Sucessfuly");
                //session values of the mobile device
                caps.setCapability("deviceName", config.getProperty("deviceName" + Integer.toString(numDevice)));
                caps.setCapability("udid", config.getProperty("udid" + Integer.toString(numDevice)));//id mobile
                caps.setCapability("platformName", config.getProperty("platformName" + Integer.toString(numDevice)));
                caps.setCapability("platformVersion", config.getProperty("platformVersion" + Integer.toString(numDevice)));
                caps.setCapability("appPackage", config.getProperty("appPackage" + Integer.toString(numDevice)));
                caps.setCapability("appActivity", config.getProperty("appActivity" + Integer.toString(numDevice)));
                caps.setCapability("noReset", config.getProperty("noReset" + Integer.toString(numDevice)));

                // checking if the server is up
                if (serverAppiumUp(appiumServer) == true) {
                    driver = new AndroidDriver<>(new URL(appiumServer), caps);

                } else {

                    Assert.fail("Server Appium " + appiumServer + " unable to connect");
                }
            } else {

                Assert.fail("there is not connected device:  " + config.getProperty("deviceName" + Integer.toString(numDevice)) + " or depuration USB unauthorized");
            }

        } else if (config.getProperty("device").equals("huawie")) {

            //huawei devise number 2
            numDevice = 2;
            System.out.println("RUNNING TESTS ON DEVICE ==> " + config.getProperty("deviceName" + Integer.toString(numDevice)));
            if (adbDevicesVerifications(config.getProperty("udid" + Integer.toString(numDevice))) == true) {

                System.out.println("\nDevice " + config.getProperty("deviceName" + Integer.toString(numDevice)) + " connected Sucessfuly");
                //Set the Desired Capabilities session
                caps.setCapability("deviceName", config.getProperty("deviceName" + Integer.toString(numDevice)));
                caps.setCapability("udid", config.getProperty("udid" + Integer.toString(numDevice)));//Give Device ID of your mobile phone
                caps.setCapability("platformName", config.getProperty("platformName" + Integer.toString(numDevice)));
                caps.setCapability("platformVersion", config.getProperty("platformVersion" + Integer.toString(numDevice)));

                caps.setCapability("app", "/Users/macminidev2/Downloads/telegram.apk");
                //caps.setCapability("androidInstallTimeout", 50000);
                caps.setCapability("appPackage", "org.telegram.messenger");
                caps.setCapability("appActivity", "org.telegram.ui.LaunchActivity");

                /*
                caps.setCapability("appPackage", config.getProperty("appPackage" + Integer.toString(numDevice)));
                caps.setCapability("appActivity", config.getProperty("appActivity" + Integer.toString(numDevice)));
                caps.setCapability("noReset", config.getProperty("noReset" + Integer.toString(numDevice)));
                caps.setCapability("orientation", "PORTRAIT");*/
                //verificando el servidor appium return true and false
                if (serverAppiumUp(appiumServer)) {

                    driver = new AndroidDriver<>(new URL(appiumServer), caps);

                    System.out.println("\nGet Orientacion: " + driver.getOrientation());

                } else {

                    Assert.fail("Server Appium " + appiumServer + " unable to connect");
                }

            } else {

                Assert.fail("there is not connected device:  " + config.getProperty("deviceName" + Integer.toString(numDevice)) + " or depuration USB unauthorized");
            }

        } else if (config.getProperty("device").equals("sansung")) {
            //huawei devise number 3
            numDevice = 3;
            System.out.println("RUNNING TESTS ON DEVICE ==> " + config.getProperty("deviceName" + Integer.toString(numDevice)));
            if (adbDevicesVerifications(config.getProperty("udid" + Integer.toString(numDevice))) == true) {
                System.out.println("\nDevice " + config.getProperty("deviceName" + Integer.toString(numDevice)) + " connected Sucessfuly");
               
                caps.setCapability("deviceName", config.getProperty("deviceName" + Integer.toString(numDevice)));
                caps.setCapability("udid", config.getProperty("udid" + Integer.toString(numDevice)));//Give Device ID of your mobile phone
                caps.setCapability("platformName", config.getProperty("platformName" + Integer.toString(numDevice)));
                caps.setCapability("platformVersion", config.getProperty("platformVersion" + Integer.toString(numDevice)));
                caps.setCapability("appPackage", config.getProperty("appPackage" + Integer.toString(numDevice)));
                caps.setCapability("appActivity", config.getProperty("appActivity" + Integer.toString(numDevice)));
                caps.setCapability("noReset", config.getProperty("noReset" + Integer.toString(numDevice)));

                //verificando servidor de appium
                if (serverAppiumUp(appiumServer) == true) {
                    driver = new AndroidDriver(new URL(appiumServer), caps);

                } else {

                    Assert.fail("Server Appium " + appiumServer + " unable to connect");
                }
            } else {

                Assert.fail("there is not connected device:  " + config.getProperty("deviceName" + Integer.toString(numDevice)) + " or depuration USB unauthorized");
            }
        }
        System.out.println("\nRUNNING THE TEST CASES:  ");

    }

    //action 
    public void methoScroll(int startx, int starty, int endx, int endy) {
        /*
        TouchAction touchAction = new TouchAction(driver);

        touchAction.longPress(PointOption.point(startx, starty))
                .moveTo(PointOption.point(endx, endy))
                .release()
                .perform();*/
    }

    //action scroll Dawn
    public void scrollDawn(int turns) {
        //The viewing size of the device
        /*  Dimension size = driver.manage().window().getSize();

        for (int i = 1; i <= turns; i++) {

            //Starting y location set to 90% of the height (near bottom)
            int starty = (int) (size.height * 0.90);
            //Ending y location set to 0.05% of the height (near top)
            int endy = (int) (size.height * 0.05);

            //x position set to mid-screen horizontally 
            int startx = (int) size.width / 2;*/

 /*
        print result
        System.err.println("startx ---> " +startx );
        System.err.println("starty ---> " +starty );
        System.err.println("startx ---> " +startx );
        System.err.println("endy ---> " + endy );
        System.err.println("total ---> " + size +" --> " + size.height);
             
            methoScroll(startx, starty, startx, endy);
            System.out.println("primera vuelta --> " + i);
        }
    }

    public void scrollUp(int turns) {
        //The viewing size of the device
        Dimension size = driver.manage().window().getSize();

        for (int i = 1; i <= turns; i++) {

        }
        //Starting y location set to 20% of the height (near bottom)
        int starty = (int) (size.height * 0.05);
        //Ending y location set to 80% of the height (near top)
        int endy = (int) (size.height * 0.90);
        //x position set to mid-screen horizontally
        int startx = size.width / 2;

        methoScroll(startx, starty, startx, endy);
    }

    public void swipeLeft(int turns) {
        //The viewing size of the device
        Dimension size = driver.manage().window().getSize();

        for (int i = 1; i <= turns; i++) {
            //Starting x location set to 95% of the width (near right)
            int startx = (int) (size.width * 0.95);
            //Ending x location set to 5% of the width (near left)
            int endx = (int) (size.width * 0.05);
            //y position set to mid-screen vertically
            int starty = size.height / 2;

            methoScroll(startx, starty, endx, starty);
        }
    }

    public void swipeRight(int turns) {
        //The viewing size of the device
        Dimension size = driver.manage().window().getSize();

        for (int i = 1; i <= turns; i++) {

            //Starting x location set to 5% of the width (near left)
            int startx = (int) (size.width * 0.05);
            //Ending x location set to 95% of the width (near right)
            int endx = (int) (size.width * 0.95);
            //y position set to mid-screen vertically
            int starty = size.height / 2;

            methoScroll(startx, starty, endx, starty);
        }*/
    }

    @AfterSuite
    public void finish() throws InterruptedException {

        if (false) {

            System.out.println("\nREMOVIENDO LA APP: " + driver.getSessionDetails().get("appPackage"));
            driver.removeApp((String) driver.getSessionDetails().get("appPackage"));
        }
        for (int i = 10; i >= 0; i--) {
            Thread.sleep(1000);
            System.out.println("Cerrando Session en: " + i);
        }
        driver.closeApp();
        driver.quit();
        System.out.println("\nSE TERMINO EL TEST:");
    }
}
